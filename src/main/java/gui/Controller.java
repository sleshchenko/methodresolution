package gui;

import gui.elements.Canvas;
import gui.elements.NodeCoordinateManager;
import gui.elements.RectangleWrapper;
import gui.elements.TextFieldWrapper.AddHBox;
import gui.elements.TextFieldWrapper.HBoxBase;
import gui.elements.TextFieldWrapper.HBoxRemove;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import com.development.ResolutionFinder;
import com.form.Disjunct;
import com.resolution.ChildDisjunct;
import com.resolution.IdentifyDisjunct;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Pattern;

public class Controller implements Initializable {
    private static final int     COUNT_FIXED_NODE = 2;
    private static final Pattern letters          = Pattern.compile("[A-Z]");

    final ToggleGroup group = new ToggleGroup();
    public VBox        textFieldContainer;
    public Pane        operatorContainer;
    public ScrollPane  scroll;
    public Button      run;
    public TextField   result_field;
    public ImageView   saveButton;
    public ImageView   loadButton;
    public Label       negativeButton;
    public Label       xorButton;
    public BorderPane  root;
    public RadioButton saturation;
    public RadioButton deletion;
    public RadioButton graph;
    /*\u8444\u8743\u8658\u8660  */
//    private Pattern simpleExpression = Pattern.compile("(not)?[A-Z]+((and|or|impl|equal)(not)?[A-Z]+)*");
    private String  operations       = "(and|or|impl|equal)";
    private String  simpleExpression = "\\(?(not)?[A-Z]+(" + operations + "(not)?[A-Z]+)*\\)?";
    //    private Pattern expression = Pattern.compile(simpleExpression + "(" + operations + simpleExpression + ")*");
    private Pattern expression       = Pattern.compile(
            "\\(?(not)?[A-Z]+(\"(and|or|impl|equal)\"(not)?[A-Z]+)*\\)?((and|or|impl|equal)\\(?(not)?[A-Z]+(\"(and|or|impl|equal)\"(not)?[A-Z]+)*\\)?)*");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        saveButton.setImage(new Image(getClass().getClassLoader().getResource("images/save.png").toString()));
        loadButton.setImage(new Image(getClass().getClassLoader().getResource("images/load.png").toString()));
        //TODO MAKE FOR EACH IN CONTAINER
        saturation.setToggleGroup(group);
        deletion.setToggleGroup(group);
        graph.setToggleGroup(group);

        AddHBox box = new AddHBox(new BorderPaneCreator(textFieldContainer));
        textFieldContainer.getChildren().add(textFieldContainer.getChildren().size() - COUNT_FIXED_NODE, box);
        box.getTextField().setText("(X∨Z)∧(¬X∨Z)∧X∧Y∧(X∨Y)");
        box.getTextField().addEventFilter(KeyEvent.KEY_TYPED, new ToUpperCaseHandler());
        result_field.addEventFilter(KeyEvent.KEY_TYPED, new ToUpperCaseHandler());
        result_field.setText("Z");

        saveButton.setOnMouseClicked(mouseEvent -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Зберегти вхідні дані");
            File file = fileChooser.showSaveDialog(root.getScene().getWindow());

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                for (Node node : textFieldContainer.getChildren()) {
                    if (node instanceof HBoxBase) {
                        writer.write(((HBoxBase)node).getTextField().getText() + "\n");
                    }
                }

                writer.write(result_field.getText());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        loadButton.setOnMouseClicked(mouseEvent -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Завантажити вхідні дані");
            File file = fileChooser.showOpenDialog(root.getScene().getWindow());
            List<String> inputs = new ArrayList<>();
            if (file != null) {
                try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                    textFieldContainer.getChildren().remove(1, textFieldContainer.getChildren().size() - COUNT_FIXED_NODE);

                    String line = null;
                    while ((line = br.readLine()) != null) {
                        inputs.add(line);
                    }

                    if (inputs.size() < 2) {
                        //TODO CHECK SIZE > 2
                    }

                    AddHBox box1 = new AddHBox(new BorderPaneCreator(textFieldContainer));
                    textFieldContainer.getChildren().add(textFieldContainer.getChildren().size() - COUNT_FIXED_NODE, box1);
                    box1.getTextField().setText(inputs.get(0));

                    for (int i = 1; i < inputs.size() - 1; ++i) {
                        HBoxRemove borderPaneRemove = new HBoxRemove(event -> {
                            Node node = (Node)event.getSource();
                            ((VBox)(node.getParent()).getParent()).getChildren().remove(node.getParent());
                        });
                        borderPaneRemove.getTextField().addEventFilter(KeyEvent.KEY_TYPED, new ToUpperCaseHandler());
                        borderPaneRemove.getTextField().setText(inputs.get(i));
                        textFieldContainer.getChildren()
                                          .add(textFieldContainer.getChildren().size() - COUNT_FIXED_NODE, borderPaneRemove);
                    }

                    result_field.setText(inputs.get(inputs.size() - 1));
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        AddedOperator addedOperator = new AddedOperator();
        for (Node node : operatorContainer.getChildren()) {
            node.addEventHandler(MouseEvent.MOUSE_CLICKED, addedOperator);
            makeClickable(node);
        }

        run.addEventHandler(MouseEvent.MOUSE_CLICKED, new FindResultStarter());

        makeClickable(saveButton);
        makeClickable(loadButton);
    }

    public static void makeClickable(Node node, Node target) {
        target.setOpacity(Controller.DEFAULT_OPACITY);
        node.setCursor(Cursor.HAND);
        node.setOnMouseEntered(e -> {
            target.setOpacity(Controller.MIN_OPACITY);
            target.setScaleX(Controller.SCALE_TO);
            target.setScaleY(Controller.SCALE_TO);
        });
        node.setOnMouseExited(e -> {
            target.setOpacity(Controller.DEFAULT_OPACITY);
            target.setScaleX(1D);
            target.setScaleY(1D);
        });
    }

    public static double DEFAULT_OPACITY = 1;
    public static double MIN_OPACITY     = 0.90;
    public static double SCALE_TO        = 1.2;

    public static void makeClickable(Node node) {
        node.setOpacity(DEFAULT_OPACITY);
        node.setCursor(Cursor.HAND);
        node.setOnMouseEntered(e -> {
            node.setOpacity(MIN_OPACITY);
            node.setScaleX(SCALE_TO);
            node.setScaleY(SCALE_TO);
        });
        node.setOnMouseExited(e -> {
            node.setOpacity(DEFAULT_OPACITY);
            node.setScaleX(1D);
            node.setScaleY(1D);
        });
    }

    private class AddedOperator implements EventHandler<MouseEvent> {
        @Override
        public void handle(MouseEvent event) {
            Object source = event.getSource();
            for (Node node : textFieldContainer.getChildren()) {
                if (node instanceof HBoxBase) {
                    TextField textField = ((HBoxBase)node).getTextField();
                    if (textField.isFocused()) {
                        int position = textField.getCaretPosition();
                        StackPane pane = (StackPane)source;
                        textField.insertText(position, ((Label)pane.getChildren().get(0)).getText());
                        textField.positionCaret(++position);
                        break;
                    }
                }
            }

            if (result_field.isFocused()) {
                int position = result_field.getCaretPosition();
                StackPane pane = (StackPane)source;
                result_field.insertText(position, ((Label)pane.getChildren().get(0)).getText());
                result_field.positionCaret(++position);
            }
        }
    }

    private static class ToUpperCaseHandler implements EventHandler<KeyEvent> {
        public void handle(KeyEvent event) {
            String input = event.getCharacter().toUpperCase();
            if (letters.matcher(input).find()) {
                Object object = event.getSource();
                if (object instanceof TextField) {
                    TextField textField = ((TextField)object);
                    int position = textField.getCaretPosition();
                    textField.insertText(position, input);
                    textField.positionCaret(++position);
                }
            }
            event.consume();
        }
    }

    private static class BorderPaneCreator implements EventHandler<MouseEvent> {
        private Pane pane;

        private BorderPaneCreator(Pane pane) {
            this.pane = pane;
        }

        @Override
        public void handle(MouseEvent e) {
            HBoxRemove borderPaneRemove = new HBoxRemove(event -> {
                Node node = (Node)event.getSource();
                ((VBox)(node.getParent()).getParent()).getChildren().remove(node.getParent());
            });
            borderPaneRemove.getTextField().addEventFilter(KeyEvent.KEY_TYPED, new ToUpperCaseHandler());
            pane.getChildren().add(pane.getChildren().size() - COUNT_FIXED_NODE, borderPaneRemove);
        }
    }

    private class FindResultStarter implements EventHandler<MouseEvent> {
        @Override
        public void handle(MouseEvent event) {
            List<String> inputs = new ArrayList<>();
            for (Node node : textFieldContainer.getChildren()) {
                if (node instanceof HBoxBase) {
                    inputs.add(((HBoxBase)node).getTextField().getText());
                }
            }

            ResolutionFinder resolutionFinder = new ResolutionFinder(inputs, result_field.getText());
            int type = 1;
            if (saturation.isSelected()) {
                type = 1;
            } else if (deletion.isSelected()) {
                type = 2;
            } else if (graph.isSelected()) {
                type = 3;
            }
            resolutionFinder.findResult(type);

            // InitWindow
            Stage primaryStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/canvas.fxml"));
            Parent root = null;
            try {
                root = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            primaryStage.setTitle("Результати");
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            scene.getStylesheets().add(getClass().getClassLoader().getResource("styles/styles.css").toString());
            primaryStage.setMinHeight(400);
            primaryStage.setMinWidth(700);
            ThreeDrawController controller = loader.getController();

            Map<Integer, ChildDisjunct> path = resolutionFinder.getResolutionPath().getPathAsMap();
            int maxValue = 0;
            for (Integer id : path.keySet()) {
                if (id > maxValue) {
                    maxValue = id;
                }
            }
            Set<Integer> parents = new HashSet<>();

            Canvas resultCanvas = new Canvas(" ", RectangleWrapper.startReadWithRoot(resolutionFinder.getResolutionPath().getPathAsMap(),
                                                                                     maxValue));
            parents.add(path.get(maxValue).getParentsId()[0]);
            parents.add(path.get(maxValue).getParentsId()[1]);
            while (!path.containsKey(--maxValue) && maxValue > 1) {
            }
            resultCanvas.setStyle("-fx-background-color: #00B1F8;");
            controller.tabContainer.getTabs().add(resultCanvas);
            NodeCoordinateManager.assignToPane(resultCanvas.getPane(), resultCanvas.getRoot());

            while (maxValue > 1) {
                if (path.get(maxValue).getParentsId().length == 2) {
                    if (!parents.contains(maxValue)) {
                        Canvas canvas =
                                new Canvas("  ",
                                           RectangleWrapper
                                                   .startReadWithRoot(resolutionFinder.getResolutionPath().getPathAsMap(), maxValue));
                        controller.tabContainer.getTabs().add(canvas);
                        NodeCoordinateManager.assignToPane(canvas.getPane(), canvas.getRoot());
                    }

                    parents.add(path.get(maxValue).getParentsId()[0]);
                    parents.add(path.get(maxValue).getParentsId()[1]);
                }
                while (!path.containsKey(--maxValue) && maxValue > 1) {
                }
            }

            controller.path = resolutionFinder.getResolutionPath();
            controller.updateTextOut();

            primaryStage.show();
            controller.redraw();
        }
    }
}