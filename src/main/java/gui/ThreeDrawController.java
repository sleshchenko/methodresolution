package gui;

import gui.elements.Canvas;
import gui.elements.Console;
import gui.elements.NodeCoordinateManager;
import gui.elements.RectangleWrapper;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

import com.resolution.IdentifyDisjunct;
import com.resolution.ResolutionPath;

import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ThreeDrawController implements Initializable {
    public ResolutionPath path;
    public CheckBox       simpleOut;
    public BorderPane     root;
    public TabPane        tabContainer;
    public ImageView      saveButton;
    public Label          saveButtonCaption;
    public ScrollPane     consoleWrapper;
    public Console        console;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Controller.makeClickable(saveButton);
        Controller.makeClickable(saveButtonCaption, saveButton);
        simpleOut.setOnAction(event -> updateTextOut());
        console = new Console();
        consoleWrapper.setContent(console);
        console.printText(Arrays.asList("12", "dsa", "dsa", "dsa", "dsa", "dsa", "dsa"));


        saveButton.setOnMouseClicked(mouseEvent -> save());

        saveButtonCaption.setOnMouseClicked(mouseEvent -> save());

    }

    public void save() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Зберегти дерево рішення");
        File file = fileChooser.showSaveDialog(root.getScene().getWindow());
        if (file == null) {
            return;
        }
        Tab tab = tabContainer.getSelectionModel().getSelectedItem();
        AnchorPane canvas = (AnchorPane)tab.getContent();
        BufferedImage image = new BufferedImage((int)canvas.getWidth(), (int)canvas.getHeight(), BufferedImage.TYPE_INT_ARGB);
        WritableImage snapshot = canvas.snapshot(new SnapshotParameters(), null);
        image = javafx.embed.swing.SwingFXUtils.fromFXImage(snapshot, image);
        try {
            Graphics2D gd = (Graphics2D)image.getGraphics();
            gd.translate(canvas.getWidth(), canvas.getHeight());
            ImageIO.write(image, "png", file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void updateTextOut() {
        console.clear();
        if (path.getResult()) {
            console.printCenter("Доведено");
            if (simpleOut.isSelected()) {
                path.simplePrint(console);
            } else {
                path.print(console);
            }
        } else {
            console.printCenter("Спростовано");
            path.print(console);
        }
        console.requestLayout();
    }

    public void redraw() {
        for (Tab tab : tabContainer.getTabs()) {
            if (tab instanceof Canvas) {
                Canvas canvas1 = (Canvas)tab;
                canvas1.getPane().requestLayout();
                NodeCoordinateManager.initCoordinate(canvas1.getRoot());
            }
        }
    }
}
