package gui.elements.TextFieldWrapper;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;

public abstract class HBoxBase extends HBox {
    protected static final Insets MARGIN = new Insets(7, 10, 7, 10);
    private static final int NODE_SIZE = 30;

    private TextField textField;
    private Button button;

    protected HBoxBase(EventHandler<MouseEvent> handler, String str) {
        textField = new TextField();
        textField.setFont(Font.font("Arial Unicode MS", 15));
        textField.setStyle("-fx-background-color: #B4E9C2;");
        setOptions(textField);
        textField.setPrefHeight(NODE_SIZE);
        getChildren().add(textField);

        Button button = new Button();
        button.setText(str);
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, handler);
        button.setPrefWidth(NODE_SIZE);
        button.setPrefHeight(NODE_SIZE);
        setOptions(button);
        getChildren().add(button);
    }

    public TextField getTextField() {
        return textField;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    public Button getButton() {
        return button;
    }

    protected void setOptions(Node node) {
        HBox.setHgrow(node, Priority.ALWAYS);
        HBox.setMargin(node, MARGIN);
    }
}
