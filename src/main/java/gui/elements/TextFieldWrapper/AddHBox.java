package gui.elements.TextFieldWrapper;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class AddHBox extends HBoxBase {
    public AddHBox(EventHandler<MouseEvent> handler) {
        super(handler, "+");
    }
}