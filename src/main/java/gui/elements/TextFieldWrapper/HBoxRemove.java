package gui.elements.TextFieldWrapper;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class HBoxRemove extends HBoxBase {
    public HBoxRemove(EventHandler<MouseEvent> handler) {
        super(handler, "-");
    }
}
