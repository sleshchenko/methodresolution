package gui.elements;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;

import com.resolution.IdentifyDisjunct;

public class Canvas extends Tab {
    private RectangleWrapper<IdentifyDisjunct> root;
    private AnchorPane                         pane;

    public Canvas(String label, RectangleWrapper<IdentifyDisjunct> root) {
        this.root = root;
        pane = new AnchorPane();
        setClosable(false);
        setContent(pane);
        setText(label);
    }

    public AnchorPane getPane() {
        return pane;
    }

    public ObservableList<Node> getChildren() {
        return pane.getChildren();
    }

    public RectangleWrapper<IdentifyDisjunct> getRoot() {
        return root;
    }
}
