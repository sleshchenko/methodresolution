package gui.elements;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;

import com.resolution.ChildDisjunct;
import com.resolution.IdentifyDisjunct;
import com.resolution.ResolutionPath;

import java.util.Map;

public class RectangleWrapper<T> {
    private static final int MARGIN_BETWEEN_NODE = 20;
    private static final int HEIGHT_ONE_LEVEL    = 50;
    private static final int X_OFFSET            = 20;
    private static final int Y_OFFSET            = 20;

    private final Label     label;
    private final StackPane wrapper;
    private       T         node;
    private       Line      toParent;

    private double xPosition;
    private float  yPosition;
    private double width;

    private RectangleWrapper<T> parent;
    private boolean             isLeftChild;
    private RectangleWrapper<T> left;
    private RectangleWrapper<T> right;

    public RectangleWrapper(T node, boolean input) {
        this.node = node;

        label = new Label();
        label.setText(node.toString());
        label.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        label.setPadding(new Insets(2, 3, 2, 4));

        wrapper = new StackPane();
        wrapper.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        wrapper.getChildren().add(label);
        wrapper.setMinWidth(40);
        ChildDisjunct asChild = (ChildDisjunct)node;

        //FIXME
        switch (asChild.getType()) {
            case INPUT:
                wrapper.getStyleClass().add("node_input");
                break;
            case RESOLUTION:
                wrapper.getStyleClass().add("node");
                break;
            case RESULT:
                wrapper.getStyleClass().add("node_result");
                break;
        }
        wrapper.setPadding(new Insets(3, 3, 3, 3));
        wrapper.setVisible(false);
        toParent = new Line();
    }

    public RectangleWrapper<T> getLeft() {
        return left;
    }

    public void setLeft(RectangleWrapper<T> left) {
        this.left = left;
        this.left.setParent(this, true);
    }

    public Label getLabel() {
        return label;
    }

    public RectangleWrapper<T> getRight() {
        return right;
    }

    public void setRight(RectangleWrapper<T> right) {
        this.right = right;
        this.right.setParent(this, false);
    }

    public void setParent(RectangleWrapper<T> parent, boolean isLeftChild) {
        this.parent = parent;
        this.isLeftChild = isLeftChild;
    }

    public StackPane getWrapper() {
        return wrapper;
    }

    public void setNode(T node) {
        this.node = node;
        label.setText(node.toString());
    }

    public T getNode() {
        return node;
    }

    public double getWidth() {
        return width;
    }

    public void calculateCoordinate() {
        if (parent == null) {
            RectangleWrapper<T> current = this;
            while (current.getLeft() != null) {
                current = current.getLeft();
            }
            xPosition =
                    X_OFFSET + (getLeft() != null ? getLeft().getWidth() - wrapper.getWidth() / 2 + current.getWrapper().getWidth() / 2 +
                                                    MARGIN_BETWEEN_NODE / 2 : 0);
        } else {
            double offset;
            if (isLeftChild) {
                offset = -1 * ((right == null) ? MARGIN_BETWEEN_NODE / 2 + wrapper.getWidth() / 2 :
                               right.getWidth() + MARGIN_BETWEEN_NODE / 2);
            } else {
                offset = (left == null) ? MARGIN_BETWEEN_NODE / 2 + wrapper.getWidth() / 2 :
                         (left.getWidth() + MARGIN_BETWEEN_NODE / 2);
            }
            xPosition = parent.getxPosition() + offset;
        }

        yPosition = parent == null ? Y_OFFSET : parent.getyPosition() + HEIGHT_ONE_LEVEL;

        wrapper.setLayoutX(xPosition - wrapper.getWidth() / 2);
        wrapper.setLayoutY(yPosition);
        wrapper.setVisible(true);
        if (parent != null) {
            toParent.setStartX(parent.getxPosition());
            toParent.setStartY(parent.getyPosition() + parent.getWrapper().getHeight());
            toParent.setEndX(xPosition);
            toParent.setEndY(yPosition);
        }
    }

    public void calculateWidth() {
        width = 0;
        if (left != null || right != null) {
            double temp = left == null ? 0 : left.getWidth() + MARGIN_BETWEEN_NODE / 2;
            width += Math.max(temp, wrapper.getWidth() / 2);

            temp = right == null ? 0 : right.getWidth() + MARGIN_BETWEEN_NODE / 2;
            width += Math.max(temp, wrapper.getWidth() / 2);
        } else {
            width = wrapper.getWidth();
        }
    }

    public double getxPosition() {
        return xPosition;
    }

    public float getyPosition() {
        return yPosition;
    }

    public RectangleWrapper<T> getParent() {
        return parent;
    }

    public void drawAt(Pane pane) {
        pane.getChildren().addAll(wrapper, toParent);
        wrapper.setLayoutX(xPosition);
        wrapper.setLayoutX(yPosition);

    }

    public static RectangleWrapper<IdentifyDisjunct> startRead(Map<Integer, ChildDisjunct> pathAsMap) {
        int maxValue = 0;
        for (Integer id : pathAsMap.keySet()) {
            if (id > maxValue) {
                maxValue = id;
            }
        }
        ChildDisjunct qwe = pathAsMap.get(maxValue);
        return recursiveReadItem(qwe, pathAsMap);
    }

    public static RectangleWrapper<IdentifyDisjunct> startReadWithRoot(Map<Integer, ChildDisjunct> pathAsMap, int root) {
        ChildDisjunct qwe = pathAsMap.get(root);
        if (qwe == null) {
            System.out.println("Error");
        }
        return recursiveReadItem(qwe, pathAsMap);
    }


    private static RectangleWrapper<IdentifyDisjunct> recursiveReadItem(ChildDisjunct current,
                                                                        Map<Integer, ChildDisjunct> path) {
        RectangleWrapper<IdentifyDisjunct> result;
        if (!current.getParents().isEmpty()) {
            result = new RectangleWrapper<>(current, false);
        } else {
            result = new RectangleWrapper<>(current, true);
        }
        if (!current.getParents().isEmpty()) {
            if (path.get(current.getParents().get(0).getId()) == null || path.get(current.getParents().get(1).getId()) == null) {
                System.out.println("Error");
            }
            result.setLeft(recursiveReadItem(path.get(current.getParents().get(0).getId()), path));
            result.setRight(recursiveReadItem(path.get(current.getParents().get(1).getId()), path));
        }
        return result;
    }
}