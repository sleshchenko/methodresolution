package gui.elements.iterators;

import gui.elements.RectangleWrapper;

import com.tree.ThreeNode;

import java.util.Iterator;
import java.util.Stack;

/**
 * @author Sergii Leschenko
 */
public class WrapperThreeIteratorToDown<T> implements Iterator<RectangleWrapper<T>> {
    private Stack<RectangleWrapper<T>> threeNodes = new Stack<>();
    private RectangleWrapper<T> prev;

    public WrapperThreeIteratorToDown(RectangleWrapper<T> top) {
        if (top != null) {
            threeNodes.push(top);
        }
    }

    @Override
    public boolean hasNext() {
        return !threeNodes.isEmpty() || prev.getLeft() != null || prev.getRight() != null;
    }

    @Override
    public RectangleWrapper<T> next() {
        if (prev != null) {
            if (prev.getLeft() != null) {
                threeNodes.push(prev.getLeft());
            }

            if (prev.getRight() != null) {
                threeNodes.push(prev.getRight());
            }
        }

        prev = threeNodes.pop();
        return prev;
    }
}