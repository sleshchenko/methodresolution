package gui.elements.iterators;

import gui.elements.RectangleWrapper;

import com.tree.ThreeNode;

import java.util.Iterator;
import java.util.Stack;

/**
 * @author Sergii Leschenko
 */
public class WrapperThreeIteratorToUp<T> implements Iterator<RectangleWrapper<T>> {
    private Stack<RectangleWrapper<T>> threeNodes = new Stack<>();

    public WrapperThreeIteratorToUp(RectangleWrapper<T> top) {
        Stack<RectangleWrapper<T>> previewStack = new Stack<>();
        previewStack.push(top);
        threeNodes.push(top);
        while (!previewStack.isEmpty()) {
            RectangleWrapper<T> threeNode = previewStack.pop();
            if (threeNode.getLeft() != null) {
                previewStack.push(threeNode.getLeft());
                threeNodes.push(threeNode.getLeft());
            }
            if (threeNode.getRight() != null) {
                previewStack.push(threeNode.getRight());
                threeNodes.push(threeNode.getRight());
            }
        }
    }

    @Override
    public boolean hasNext() {
        return !threeNodes.isEmpty();
    }

    @Override
    public RectangleWrapper<T> next() {
        return threeNodes.pop();
    }
}

