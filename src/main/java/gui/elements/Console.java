package gui.elements;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import com.resolution.IdentifyDisjunct;

import java.util.Arrays;
import java.util.List;

public class Console extends VBox {
    public String str;

    public void printText(List<String> text) {
        for (String s : text) {
            getChildren().add(getLabel(s));
        }
    }

    public void printLine(String str) {
        getChildren().add(getLabel(str));
    }

    public void clear() {
        getChildren().clear();
    }

    private AnchorPane getLabel(String str) {
        AnchorPane pane = new AnchorPane();
        Label line = new Label(str);
        VBox.setMargin(line, new Insets(1.5, 5, 1.5, 5));
        line.setMinWidth(205);

        line.setFont(Font.font("Comic Sans MS", 14));
        pane.getChildren().add(line);
        return pane;
    }

    public void printCenter(String str) {
        AnchorPane pane = getLabel(str);
        ((Label)pane.getChildren().get(0)).setAlignment(Pos.BOTTOM_CENTER);
        getChildren().add(pane);
    }


    public void print(IdentifyDisjunct identifyDisjunct) {
        AnchorPane pane = getLabel(identifyDisjunct.getFullString());

        switch (identifyDisjunct.getType()) {
            case INPUT:
                pane.getStyleClass().add("dis_input");
                break;
            case RESOLUTION:
                pane.getStyleClass().add("dis");
                break;
            case RESULT:
                pane.getStyleClass().add("dis_result");
                break;
        }
        getChildren().add(pane);
    }
}
