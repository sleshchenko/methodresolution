package gui.elements;

import gui.elements.iterators.WrapperThreeIteratorToDown;
import gui.elements.iterators.WrapperThreeIteratorToUp;
import javafx.scene.layout.Pane;

public class NodeCoordinateManager {
    public static void initCoordinate(RectangleWrapper root) {
        WrapperThreeIteratorToUp iteratorToUp = new WrapperThreeIteratorToUp(root);
        while (iteratorToUp.hasNext()) {
            iteratorToUp.next().calculateWidth();
        }

        WrapperThreeIteratorToDown iteratorToDown = new WrapperThreeIteratorToDown(root);
        while (iteratorToDown.hasNext()) {
            iteratorToDown.next().calculateCoordinate();
        }
    }

    public static void assignToPane(Pane pane, RectangleWrapper root) {
        WrapperThreeIteratorToDown iteratorToDown = new WrapperThreeIteratorToDown(root);
        while (iteratorToDown.hasNext()) {
            iteratorToDown.next().drawAt(pane);
        }
    }
}
