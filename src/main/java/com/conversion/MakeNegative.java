package com.conversion;

import com.expression.ExpressionItem;
import com.expression.operation.Operator;
import com.tree.ThreeNode;

/**
 * Use it for get negation for all expression
 *
 * @author Sergii Leschenko
 */
public class MakeNegative implements Converter {
    @Override
    public boolean convert(ThreeNode<ExpressionItem> input) {
        ThreeNode<ExpressionItem> tmp = new ThreeNode<>(input);
        input.reinit(Operator.NEGATION, tmp, null);
        return true;
    }
}
