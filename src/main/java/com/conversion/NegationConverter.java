package com.conversion;

import com.expression.ExpressionItem;
import com.expression.variable.Variable;
import com.expression.operation.Operator;
import com.tree.ThreeNode;

/**
 * This converter delete double negation and applies negation to variables
 *
 * @author Sergii Leschenko
 */
public class NegationConverter implements Converter {
    @Override
    public boolean convert(ThreeNode<ExpressionItem> input) {
        if (input.getValue() != Operator.NEGATION) {
            return false;
        }

        if (input.getLeft().getValue() instanceof Variable) {
            return false;
        }

        if (input.getLeft().getValue().equals(Operator.NEGATION)) {
            input.reinit(input.getLeft().getLeft());
            return true;
        }
        ThreeNode left = input.getLeft();
        input.setValue(revertOperator((Operator) left.getValue()));
        input.setLeft(new ThreeNode<ExpressionItem>(Operator.NEGATION, left.getLeft()));
        input.setRight(new ThreeNode<ExpressionItem>(Operator.NEGATION, left.getRight()));

        return true;
    }

    private Operator revertOperator(Operator operator) {
        if (Operator.CONJUCTION.equals(operator)) {
            return Operator.DISJUNCTION;
        } else {
            return Operator.CONJUCTION;
        }
    }
}
