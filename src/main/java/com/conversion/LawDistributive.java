package com.conversion;

import com.expression.ExpressionItem;
import com.expression.operation.Operator;
import com.tree.ThreeNode;

/**
 * @author Sergii Leschenko
 */
public class LawDistributive implements Converter {
    @Override
    public boolean convert(ThreeNode<ExpressionItem> input) {
        if (!input.getValue().equals(Operator.DISJUNCTION)) {
            return false;
        }
        if (input.getLeft().getValue().equals(Operator.CONJUCTION)) {
            input.setValue(Operator.CONJUCTION);
            ThreeNode first = input.getLeft().getLeft();
            ThreeNode second = input.getLeft().getRight();
            ThreeNode right = input.getRight();
            input.setLeft(getDisjuction(first, right));
            input.setRight(getDisjuction(second, right));
            return true;
        }

        if (input.getRight().getValue().equals(Operator.CONJUCTION)) {
            input.setValue(Operator.CONJUCTION);
            ThreeNode first = input.getRight().getLeft();
            ThreeNode second = input.getRight().getRight();
            ThreeNode left = input.getLeft();
            input.setLeft(getDisjuction(first, left));
            input.setRight(getDisjuction(second, left));
            return true;
        }
        return false;
    }

    private ThreeNode getDisjuction(ThreeNode<ExpressionItem> threeNode1, ThreeNode<ExpressionItem> threeNode2) {
        return new ThreeNode<>(Operator.DISJUNCTION, threeNode1, threeNode2);
    }
}
