package com.conversion;

import com.expression.ExpressionItem;
import com.expression.operation.Operator;
import com.tree.ThreeNode;

/**
 * Converter implication to diskunction
 *
 * @author Sergii Leschenko
 */
public class ImplicationConverter implements Converter {
    @Override
    public boolean convert(ThreeNode<ExpressionItem> input) {
        if (input.getValue() != Operator.IMPLICATION) {
            return false;
        }
        ThreeNode<ExpressionItem> first = new ThreeNode<>(Operator.NEGATION, input.getRight());
        input.setValue(Operator.DISJUNCTION);
        input.setRight(first);
        return true;
    }
}
