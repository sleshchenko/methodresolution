package com.conversion;

import com.expression.ExpressionItem;
import com.expression.operation.Operator;
import com.tree.ThreeNode;

/**
 * Converting equivalency to conjuction
 *
 * @author Sergii Leschenko
 */
public class EquivalencyConverter implements Converter {
    @Override
    public boolean convert(ThreeNode<ExpressionItem> input) {
        if (input.getValue() != Operator.EQUIVALENCY) {
            return false;
        }
        input.reinit(Operator.CONJUCTION,
                build(input.getLeft(), input.getRight()),
                build(input.getRight(), input.getLeft()));
        return true;
    }

    private static ThreeNode build(ThreeNode left, ThreeNode right) {
        ThreeNode<ExpressionItem> first = new ThreeNode<>(Operator.NEGATION, left.copy());
        return new ThreeNode<ExpressionItem>(Operator.DISJUNCTION, first, right.copy());
    }
}
