package com.conversion;

import com.expression.ExpressionItem;
import com.expression.operation.Operator;
import com.tree.ThreeNode;

public class XorConverter implements Converter {
    @Override
    public boolean convert(ThreeNode<ExpressionItem> input) {
        if (input.getValue() != Operator.XOR) {
            return false;
        }

        ThreeNode<ExpressionItem> qwe = new ThreeNode<>(Operator.EQUIVALENCY, input.getLeft(), input.getRight());
        input.reinit(Operator.NEGATION, qwe, null);

        return true;
    }
}
