package com.conversion;

import com.expression.ExpressionItem;
import com.expression.operation.Operations;
import com.expression.operation.Operator;
import com.expression.variable.Variable;
import com.sun.corba.se.spi.orb.Operation;
import com.tree.ThreeNode;

public class ArrowConverter implements Converter {
    @Override
    public boolean convert(ThreeNode<ExpressionItem> input) {
        if (input.getValue() != Operator.ARROW) {
            return false;
        }

        ThreeNode<ExpressionItem> qwe = new ThreeNode<>(Operator.DISJUNCTION, input.getLeft(), input.getRight());
        input.reinit(Operator.NEGATION, qwe, null);

        return true;
    }
}
