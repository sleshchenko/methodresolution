package com.conversion;

import com.expression.ExpressionItem;
import com.tree.ThreeNode;

/**
 * Interface for converting nodes
 *
 * @author Sergii Leshchenko
 */
public interface Converter {
    public boolean convert(ThreeNode<ExpressionItem> input);
}
