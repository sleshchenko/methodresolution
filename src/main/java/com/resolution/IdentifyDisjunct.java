package com.resolution;

import com.expression.variable.Variable;
import com.form.Disjunct;

public class IdentifyDisjunct extends Disjunct {
    private int id;

    public IdentifyDisjunct(int id, Type type) {
        super(type);
        this.id = id;
    }

    public IdentifyDisjunct(int id, Type type, Variable... vars) {
        super(type, vars);
        this.id = id;
    }

    public IdentifyDisjunct(int id, Disjunct prototype) {
        super(prototype, prototype.getType());
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString() {
        return super.toString();
    }

    public String getFullString() {
        return id + "( " + super.toString() + " )";
    }

    public boolean equals(Object o) {
        if (!(o instanceof IdentifyDisjunct)) {
            return false;
        }

        return super.equals(o);
    }
}
