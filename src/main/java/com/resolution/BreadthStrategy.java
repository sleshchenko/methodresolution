package com.resolution;

import com.expression.variable.Variable;
import com.form.ConjunctiveNormalForm;
import com.form.Disjunct;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class BreadthStrategy implements Strategy {
    private int                    counter   = 0;
    private List<IdentifyDisjunct> disjuncts = new ArrayList<>();
    private ResolutionPath resolutionPath;

    public BreadthStrategy(Disjunct... disjuncts) {
        for (Disjunct input : disjuncts) {
            this.disjuncts.add(new IdentifyDisjunct(++counter, input));
        }
    }

    public BreadthStrategy(Collection<Disjunct> disjuncts) {
        this();
        for (Disjunct input : disjuncts) {
            this.disjuncts.add(new IdentifyDisjunct(++counter, input));
        }
    }

    public BreadthStrategy(List<ConjunctiveNormalForm> conjunctiveNormalForms) {
        Set<Disjunct> disjunctions = new HashSet<>();

        for (ConjunctiveNormalForm conjunctiveNormalForm : conjunctiveNormalForms) {
            for (Disjunct disjunct : conjunctiveNormalForm.getDisjuncts()) {
                disjunctions.add(disjunct);
            }
        }

        for (Disjunct disjunct : disjunctions) {
            this.disjuncts.add(new IdentifyDisjunct(++counter, disjunct));
        }
    }

    public boolean isBehove() {
        counter = disjuncts.size();
        resolutionPath = new ResolutionPath(disjuncts);
        List<Way> ways = new ArrayList<>();
        List<Way> completed = new ArrayList<>();
        ways.add(new Way(disjuncts));

        int currentWay = 0;
        boolean result = false;

        while (!ways.isEmpty()) {
            Way current = ways.get(currentWay);

            Way newWay = current.findNext();
            if (newWay != null) {
                ways.add(newWay);
                if (newWay.isResult()) {
                    result = true;
                    break;
                }
                ++currentWay;
            } else {
                completed.add(current);
                ways.remove(current);
                --currentWay;
            }
        }

        if (!ways.isEmpty()) {
            for (Way way : ways) {
                completed.add(way);
            }
        }

        for (Way way : completed) {
            System.out.println(way.toString());
        }

        return result;
    }

    public ResolutionPath getResolutionPath() {
        return resolutionPath;
    }

    private class Way {
        List<IdentifyDisjunct> disjuncts = new ArrayList<>();
        Iterator<Variable> cursorVar;
        int                resolvent;
        int                cursorItem;

        public Way(List<IdentifyDisjunct> disjuncts) {
            this.disjuncts.addAll(disjuncts);
            cursorItem = 1;
            resolvent = 0;
            cursorVar = disjuncts.get(cursorItem).getVars().iterator();
        }

        public Way findNext() {
            for (; cursorItem < disjuncts.size(); ++cursorItem) {
                for (; resolvent >= 0; --resolvent) {
                    IdentifyDisjunct first = disjuncts.get(cursorItem);
                    IdentifyDisjunct second = disjuncts.get(resolvent);

                    while (cursorVar.hasNext()) {
                        Variable variable = cursorVar.next();
                        Variable negation = variable.getNegation();
                        if (second.containsVariable(negation)) {
                            ResolutionLaw resolutionLaw = new ResolutionLaw(first, second, variable);

                            ChildDisjunct newDisjunct = new ChildDisjunct(++counter, resolutionLaw.getResult(), first, second);
                            if (!disjuncts.contains(newDisjunct)) {
                                resolutionPath.addResolution(newDisjunct);
                                List<IdentifyDisjunct> newDisjuncts = new ArrayList<>(disjuncts);
                                newDisjuncts.add(newDisjunct);
                                return new Way(newDisjuncts);
                            }
                        }
                    }

                    cursorVar = first.getVars().iterator();
                }
                resolvent = cursorItem-1;
            }
            return null;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();

            sb.append("Vars ");
            for (IdentifyDisjunct disjunct : disjuncts) {
                sb.append(disjunct.getFullString() + "|");
            }

            return sb.toString();
        }

        public boolean isResult() {
            return disjuncts.get(disjuncts.size()-1).getVars().isEmpty();
        }
    }
}
