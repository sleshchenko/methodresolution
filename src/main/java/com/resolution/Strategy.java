package com.resolution;

public interface Strategy {
    public boolean isBehove();

    ResolutionPath getResolutionPath();
}
