package com.resolution;

import com.expression.variable.Variable;
import com.form.ConjunctiveNormalForm;
import com.form.Disjunct;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SmallStrategy implements Strategy {
    private int                    counter   = 0;
    private List<IdentifyDisjunct> disjuncts = new ArrayList<>();
    private ResolutionPath resolutionPath;

    public SmallStrategy(Disjunct... disjuncts) {
        for (Disjunct input : disjuncts) {
            this.disjuncts.add(new IdentifyDisjunct(++counter, input));
        }
    }

    public SmallStrategy(Collection<Disjunct> disjuncts) {
        this();
        for (Disjunct input : disjuncts) {
            this.disjuncts.add(new IdentifyDisjunct(++counter, input));
        }
    }

    public SmallStrategy(List<ConjunctiveNormalForm> conjunctiveNormalForms) {
        Set<Disjunct> disjunctions = new HashSet<>();

        for (ConjunctiveNormalForm conjunctiveNormalForm : conjunctiveNormalForms) {
            for (Disjunct disjunct : conjunctiveNormalForm.getDisjuncts()) {
                disjunctions.add(disjunct);
            }
        }

        for (Disjunct disjunct : disjunctions) {
            this.disjuncts.add(new IdentifyDisjunct(++counter, disjunct));
        }
    }

    public boolean isBehove() {
        counter = disjuncts.size();

        disjuncts.sort((o1, o2) -> {
            if (o1.getVars().size() > o2.getVars().size()) {
                return 1;
            }
            if (o1.getVars().size() < o2.getVars().size()) {
                return -1;
            }
            return 0;
        });

        resolutionPath = new ResolutionPath(disjuncts);
        for (int i = 1; i < disjuncts.size(); ++i) {
            for (int j = i - 1; j >= 0; --j) {
                IdentifyDisjunct first = disjuncts.get(i);
                IdentifyDisjunct second = disjuncts.get(j);
                for (Variable variable : first.getVars()) {
                    Variable negation = variable.getNegation();
                    if (second.containsVariable(negation)) {
                        ResolutionLaw resolutionLaw = new ResolutionLaw(first, second, variable);
                        ChildDisjunct newDisjunct = new ChildDisjunct(++counter, resolutionLaw.getResult(), first, second);
                        resolutionPath.addResolution(newDisjunct);

                        if (newDisjunct.getVars().isEmpty()) {
                            return true;
                        }

                        if (!disjuncts.contains(newDisjunct)) {
                            disjuncts.add(j + 1, newDisjunct);
                        }
                    }
                }
            }
        }
        return false;
    }

    public ResolutionPath getResolutionPath() {
        return resolutionPath;
    }
}
