package com.resolution;

import com.form.Disjunct;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ChildDisjunct extends IdentifyDisjunct {
    private static int COUNT_PARENT = 2;

    private List<IdentifyDisjunct> parents = new ArrayList<>(COUNT_PARENT);

    public ChildDisjunct(int id, Disjunct value, IdentifyDisjunct disjunct1,
                         IdentifyDisjunct disjunct2) {
        super(id, value);
        parents.add(disjunct1);
        parents.add(disjunct2);
    }

    public ChildDisjunct(int id, Disjunct value) {
        super(id, value);
        parents = new ArrayList<>();
    }

    public String getFullString() {
        if (parents.isEmpty()) {
            return super.toString();
        }
        return super.getFullString() + " з " + parents.get(0).getId() + " & " + parents.get(1).getId();
    }

    public int[] getParentsId() {
        List<Integer> result = new ArrayList<>();
        for (IdentifyDisjunct identifyDisjunct : parents) {
            result.add(identifyDisjunct.getId());
        }

        int[] resultint = new int[result.size()];

        for (int i = 0; i < result.size(); ++i) {
            resultint[i] = result.get(i);
        }
        return resultint;
    }

    public List<IdentifyDisjunct> getParents() {
        return parents;
    }
}
