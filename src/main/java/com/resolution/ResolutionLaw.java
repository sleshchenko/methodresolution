package com.resolution;

import com.expression.variable.Variable;
import com.form.Disjunct;

public class ResolutionLaw {
    private Disjunct first;
    private Disjunct second;
    private Variable variable;

    public ResolutionLaw(Disjunct first, Disjunct second, Variable variable) {
        this.first = first;
        this.second = second;
        this.variable = variable;
    }

    public Disjunct getResult() {
        Disjunct newDisjunct = new Disjunct(second, Disjunct.Type.RESOLUTION);
        newDisjunct.addVariables(first.getVars());
        newDisjunct.removeVariables(variable.getNegation());
        newDisjunct.removeVariables(variable);
        return newDisjunct;
    }

    public Disjunct getFirst() {
        return first;
    }

    public Disjunct getSecond() {
        return second;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(first.toString());
        sb.append(" + ");
        sb.append(second.toString());
        sb.append(" = ");
        sb.append(getResult().toString());
        sb.append(" by ");
        sb.append(variable);
        return sb.toString();
    }
}
