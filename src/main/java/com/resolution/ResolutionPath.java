package com.resolution;

import gui.elements.Console;

import com.expression.ExpressionItem;
import com.tree.Three;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class ResolutionPath {
    Set<IdentifyDisjunct> inputs         = new HashSet<>();
    List<ChildDisjunct>   childDisjuncts = new ArrayList<>();

    public boolean result;

    public ResolutionPath(Collection<IdentifyDisjunct> inputs) {
        this.inputs.addAll(inputs);
    }

    public Map<Integer, ChildDisjunct> getPathAsMap() {
        Map<Integer, ChildDisjunct> result = new HashMap<>();
        for (IdentifyDisjunct input : inputs) {
            result.put(input.getId(), new ChildDisjunct(input.getId(), input));
        }

        for (ChildDisjunct childDisjunct : childDisjuncts) {
            result.put(childDisjunct.getId(), childDisjunct);
        }

        return result;
    }

    public void addResolution(ChildDisjunct resolution) {
        childDisjuncts.add(resolution);
    }

    public Set<IdentifyDisjunct> getInputs() {
        return inputs;
    }

    public List<ChildDisjunct> getChildDisjuncts() {
        return childDisjuncts;
    }

    public void print(Console console) {
        console.printLine("Вхідні диз'юнкти");
        for (IdentifyDisjunct input : inputs) {
            console.print(input);
        }
        console.printLine("Виведення");
        for (ChildDisjunct resolution : childDisjuncts) {
            console.print(resolution);
        }
    }

    public void simplePrint(Console console) {
        console.printLine("Вхідні диз'юнкти");
        for (IdentifyDisjunct input : inputs) {
            console.print(input);
        }
        console.printLine("Виведення");
        Stack<ChildDisjunct> stack = new Stack<>();
        List<ChildDisjunct> finished = new ArrayList<>();
        ChildDisjunct current = childDisjuncts.get(childDisjuncts.size() - 1);
        stack.push(current);
        while (!stack.isEmpty()) {
            ChildDisjunct now = stack.pop();
            finished.add(now);
            for (IdentifyDisjunct identifyDisjunct : now.getParents()) {
                if (identifyDisjunct instanceof ChildDisjunct) {
                    stack.push((ChildDisjunct)identifyDisjunct);
                }
            }
        }
        finished.sort((o1, o2) -> new Integer(o1.getId()).compareTo(o2.getId()));
        for (IdentifyDisjunct identifyDisjunct : finished) {
            console.print(identifyDisjunct);
        }
    }

    private static class StringBuilderMultiLine {
        StringBuilder sb = new StringBuilder();

        public void append(String str) {
            sb.append(str);
        }

        public void appendLine(String str) {
            sb.append(str).append("\n");
        }

        public String toString() {
            return sb.toString();
        }
    }

    public Three<ExpressionItem> toThree() {
        Stack<ChildDisjunct> stack = new Stack<>();
        List<ChildDisjunct> finished = new ArrayList<>();
        ChildDisjunct current = childDisjuncts.get(childDisjuncts.size() - 1);
        stack.push(current);
        while (!stack.isEmpty()) {
            ChildDisjunct now = stack.pop();
            finished.add(now);
            for (IdentifyDisjunct identifyDisjunct : now.getParents()) {
                if (identifyDisjunct instanceof ChildDisjunct) {
                    stack.push((ChildDisjunct)identifyDisjunct);
                }
            }
        }
        finished.sort((o1, o2) -> new Integer(o1.getId()).compareTo(o2.getId()));


        return null;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}