package com.resolution;

import com.expression.variable.Variable;
import com.form.ConjunctiveNormalForm;
import com.form.Disjunct;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DeletionStrategy implements Strategy {
    private int                    counter   = 0;
    private List<IdentifyDisjunct> disjuncts = new ArrayList<>();
    private ResolutionPath resolutionPath;

    public DeletionStrategy(Disjunct... disjuncts) {
        for (Disjunct input : disjuncts) {
            this.disjuncts.add(new IdentifyDisjunct(++counter, input));
        }
    }

    public DeletionStrategy(Collection<Disjunct> disjuncts) {
        this();
        for (Disjunct input : disjuncts) {
            this.disjuncts.add(new IdentifyDisjunct(++counter, input));
        }
    }

    public DeletionStrategy(List<ConjunctiveNormalForm> conjunctiveNormalForms) {
        Set<Disjunct> disjunctions = new HashSet<>();

        for (ConjunctiveNormalForm conjunctiveNormalForm : conjunctiveNormalForms) {
            for (Disjunct disjunct : conjunctiveNormalForm.getDisjuncts()) {
                disjunctions.add(disjunct);
            }
        }

        for (Disjunct disjunct : disjunctions) {
            this.disjuncts.add(new IdentifyDisjunct(++counter, disjunct));
        }
    }

    public boolean isBehove() {
        counter = disjuncts.size();
        resolutionPath = new ResolutionPath(disjuncts);
        for (int i = 1; i < disjuncts.size(); ++i) {
            for (int j = i - 1; j >= 0; --j) {
                IdentifyDisjunct first = disjuncts.get(i);
                IdentifyDisjunct second = disjuncts.get(j);
                for (Variable variable : first.getVars()) {
                    Variable negation = variable.getNegation();
                    if (second.containsVariable(negation)) {
                        ResolutionLaw resolutionLaw = new ResolutionLaw(first, second, variable);
                        ChildDisjunct newDisjunct =
                                new ChildDisjunct(++counter, resolutionLaw.getResult(), first, second);
                        boolean isExtends = false;
                        for (int k = 0; k < disjuncts.size() && !isExtends ; ++k) {
                            isExtends = disjuncts.get(k).isExtendsFor(newDisjunct) || newDisjunct.isTautology();
                        }
                        if (!isExtends) {
                            resolutionPath.addResolution(newDisjunct);

                            if (newDisjunct.getVars().isEmpty()) {
                                return true;
                            }
                            if (!disjuncts.contains(newDisjunct)) {
                                disjuncts.add(newDisjunct);
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public ResolutionPath getResolutionPath() {
        return resolutionPath;
    }
}
