package com.reducing;

import com.expression.ExpressionItem;
import com.expression.variable.LogicalConstant;
import com.expression.operation.Operator;
import com.tree.ThreeNode;

/**
 * @author Sergii Leschenko
 */
public class ConstantReducer implements Reducer {
    @Override
    public boolean tryReduce(ThreeNode<ExpressionItem> threeNode) {
        if (threeNode == null || threeNode.getLeft() == null || threeNode.getRight() == null ||
                !(threeNode.getValue() instanceof Operator)) {
            return false;
        }
        Operator logicOperator = (Operator) threeNode.getValue();

        if (!logicOperator.equals(Operator.CONJUCTION) && !logicOperator.equals(Operator.DISJUNCTION)) {
            return false;
        }

        if (logicOperator.equals(Operator.CONJUCTION)) {
            if (threeNode.getLeft().getValue().equals(LogicalConstant.getFalse()) ||
                    threeNode.getRight().getValue().equals(LogicalConstant.getFalse())) {
                threeNode.reinit(LogicalConstant.getFalse(), null, null);
                return true;
            }

            if (threeNode.getLeft().getValue().equals(LogicalConstant.getTrue())) {
                threeNode.reinit(threeNode.getRight());
                return true;
            }

            if (threeNode.getRight().getValue().equals(LogicalConstant.getTrue())) {
                threeNode.reinit(threeNode.getLeft());
                return true;
            }
        }

        if (logicOperator.equals(Operator.DISJUNCTION)) {
            if (threeNode.getLeft().getValue().equals(LogicalConstant.getTrue()) ||
                    threeNode.getRight().getValue().equals(LogicalConstant.getTrue())) {
                threeNode.reinit(LogicalConstant.getTrue(), null, null);
                return true;
            }

            if (threeNode.getLeft().getValue().equals(LogicalConstant.getFalse())) {
                threeNode.reinit(threeNode.getRight());
                return true;
            }

            if (threeNode.getRight().getValue().equals(LogicalConstant.getFalse())) {
                threeNode.reinit(threeNode.getLeft());
                return true;
            }
        }

        return false;
    }

}
