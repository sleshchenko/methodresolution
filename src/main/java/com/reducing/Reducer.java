package com.reducing;

import com.expression.ExpressionItem;
import com.tree.ThreeNode;

/**
 * @author Sergii Leschenko
 */
public interface Reducer {
    public boolean tryReduce(ThreeNode<ExpressionItem> threeNode);
}
