package com.reducing;

import com.expression.ExpressionItem;
import com.expression.operation.Operator;
import com.expression.variable.LogicalConstant;
import com.expression.variable.Variable;
import com.tree.ThreeNode;
import com.tree.iterator.SubThreeIteratorBySpecificOperator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author Sergii Leschenko
 */
public class VariablesReducer implements Reducer {

    @Override
    public boolean tryReduce(ThreeNode<ExpressionItem> threeNode) {
        if (threeNode == null || threeNode.getLeft() == null || threeNode.getRight() == null ||
                !(threeNode.getValue() instanceof Operator)) {
            return false;
        }
        Operator logicOperator = (Operator) threeNode.getValue();

        if (!logicOperator.equals(Operator.CONJUCTION) && !logicOperator.equals(Operator.DISJUNCTION)) {
            return false;
        }

        List<ThreeNode<ExpressionItem>> variables = getVariables(threeNode, logicOperator);
        buildTree(threeNode, variables, logicOperator);

        return true;
    }

    private List<ThreeNode<ExpressionItem>> getVariables(ThreeNode<ExpressionItem> threeNode, Operator operator) {
        Set<Variable> positive = new HashSet<>();
        Set<Variable> negative = new HashSet<>();
        List<ThreeNode<ExpressionItem>> result = new ArrayList<>();

        Iterator<ThreeNode<ExpressionItem>> iterator = new SubThreeIteratorBySpecificOperator(threeNode, operator);
        while (iterator.hasNext()) {
            ThreeNode<ExpressionItem> threeNode1 = iterator.next();
            ExpressionItem item = threeNode1.getValue();
            if (item instanceof Variable) {
                positive.add((Variable) item);
            } else if (item.equals(Operator.NEGATION) && threeNode1.getLeft().getValue() instanceof Variable) {
                negative.add((Variable) threeNode1.getLeft().getValue());
            } else if (!item.equals(operator)) {
                result.add(threeNode1);
            }
        }

        Set<Variable> variables = new HashSet<>();
        boolean isBreak = false;
        for (Variable variable : positive) {
            if (negative.contains(variable)) {
                variables = new HashSet<>();
                variables.add(addNegativePositive(operator));
                isBreak = true;
                break;
            } else {
                variables.add(variable);
                result.add(new ThreeNode<>(variable));
            }
        }

        if (!isBreak) {
            for (Variable variable : negative) {
                if (!positive.contains(variable)) {
                    variables.add(variable);
                    result.add(new ThreeNode<>(Operator.NEGATION, new ThreeNode<>(variable)));
                }
            }
        }

        if (isBreak) {
            result = new ArrayList<>();
            for (Variable logicVariable : variables) {
                result.add(new ThreeNode<>(logicVariable));
            }
        }

        return result;
    }

    private LogicalConstant addNegativePositive(Operator operator) {
        if (operator.equals(Operator.CONJUCTION)) {
            return LogicalConstant.getFalse();
        } else if (operator.equals(Operator.DISJUNCTION)) {
            return LogicalConstant.getTrue();
        }

        return null;
    }

    private void buildTree(ThreeNode<ExpressionItem> threeNode, List<ThreeNode<ExpressionItem>> items, Operator operator) {
        if (items.isEmpty()) {
            return;
        }

        Iterator<ThreeNode<ExpressionItem>> iterator = items.iterator();

        if (items.size() == 1) {
            threeNode.reinit(iterator.next());
            return;
        }

        ThreeNode<ExpressionItem> prev = threeNode;
        prev.setValue(operator);
        for (int i = 0; i < items.size() - 2; ++i) {
            prev.setRight(iterator.next());
            ThreeNode<ExpressionItem> tmp = new ThreeNode<>(operator);
            prev.setLeft(tmp);
            prev = tmp;
        }

        prev.setLeft(iterator.next());

        prev.setRight(iterator.next());
    }
}
