package com.tree;

import com.tree.iterator.ThreeIteratorToDown;
import com.tree.iterator.ThreeIteratorToDownNodeWithLevel;

import java.util.Iterator;

/**
 * @author Sergii Leschenko
 */
public class Three<T> implements Iterable<ThreeNode<T>> {
    private ThreeNode<T> top;

    public Three(ThreeNode<T> top) {
        this.top = top;
    }

    public ThreeNode<T> getTop() {
        return top;
    }

    public void setTop(ThreeNode<T> top) {
        this.top = top;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (ThreeNode threeNode : this) {
            sb.append(threeNode);
        }

        return sb.reverse().toString();
    }


    public void print() {
        ThreeIteratorToDownNodeWithLevel qwe = new ThreeIteratorToDownNodeWithLevel<>(top);

        while (qwe.hasNext()) {
            ThreeNodeWithLevel nodeWithLevel = qwe.next();
            for (int i = 1; i <= nodeWithLevel.getLevel(); i++) {
                System.out.print("\t");
            }
            System.out.println(nodeWithLevel.getValue());
        }
    }

    @Override
    public Iterator<ThreeNode<T>> iterator() {
        return new ThreeIteratorToDown<>(top);
    }
}
