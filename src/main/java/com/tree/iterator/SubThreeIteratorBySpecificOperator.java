package com.tree.iterator;

import com.expression.ExpressionItem;
import com.expression.operation.Operator;
import com.tree.ThreeNode;

import java.util.Iterator;
import java.util.Stack;

/**
 * @author Sergii Leschenko
 */
public class SubThreeIteratorBySpecificOperator implements Iterator<ThreeNode<ExpressionItem>> {
    private Stack<ThreeNode<ExpressionItem>> threeNodes = new Stack<>();
    private Operator                  operator;
    private ThreeNode<ExpressionItem> prev;

    public SubThreeIteratorBySpecificOperator(ThreeNode<ExpressionItem> top, Operator operator) {
        if (top != null) {
            threeNodes.push(top);
        }
        this.operator = operator;
    }

    @Override
    public boolean hasNext() {
        return !threeNodes.isEmpty() || (prev.getValue().equals(operator) && prev.getLeft() != null);
    }

    @Override
    public ThreeNode<ExpressionItem> next() {
        if (prev != null && prev.getValue().equals(operator)) {
            if (prev.getLeft() != null) {
                threeNodes.push(prev.getLeft());
            }

            if (prev.getRight() != null) {
                threeNodes.push(prev.getRight());
            }
        }

        prev = threeNodes.pop();
        return prev;
    }
}

