package com.tree.iterator;

import com.tree.ThreeNode;

import java.util.Iterator;
import java.util.Stack;

/**
 * @author Sergii Leschenko
 */
public class ThreeIteratorToUp<T> implements Iterator<ThreeNode<T>> {
    private Stack<ThreeNode> threeNodes = new Stack<>();

    public ThreeIteratorToUp(ThreeNode top) {
        Stack<ThreeNode> tmp = new Stack<>();
        tmp.push(top);
        threeNodes.push(top);
        while (!tmp.isEmpty()) {
            ThreeNode threeNode = tmp.pop();
            if (threeNode.getLeft() != null) {
                tmp.push(threeNode.getLeft());
                threeNodes.push(threeNode.getLeft());
            }
            if (threeNode.getRight() != null) {
                tmp.push(threeNode.getRight());
                threeNodes.push(threeNode.getRight());
            }
        }
    }

    @Override
    public boolean hasNext() {
        return !threeNodes.isEmpty();
    }

    @Override
    public ThreeNode next() {
        return threeNodes.pop();
    }
}

