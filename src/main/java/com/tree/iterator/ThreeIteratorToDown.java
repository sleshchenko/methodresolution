package com.tree.iterator;

import com.tree.ThreeNode;

import java.util.Iterator;
import java.util.Stack;

/**
 * @author Sergii Leschenko
 */
public class ThreeIteratorToDown<T> implements Iterator<ThreeNode<T>> {
    private Stack<ThreeNode> threeNodes = new Stack<>();
    private ThreeNode prev;

    public ThreeIteratorToDown(ThreeNode top) {
        if (top != null) {
            threeNodes.push(top);
        }
    }

    @Override
    public boolean hasNext() {
        return !threeNodes.isEmpty() || prev.getLeft() != null;
    }

    @Override
    public ThreeNode<T> next() {
        if (prev != null) {
            if (prev.getLeft() != null) {
                threeNodes.push(prev.getLeft());
            }

            if (prev.getRight() != null) {
                threeNodes.push(prev.getRight());
            }
        }

        prev = threeNodes.pop();
        return prev;
    }
}