package com.tree.iterator;

import com.tree.ThreeNode;
import com.tree.ThreeNodeWithLevel;

import java.util.Iterator;
import java.util.Stack;

public class ThreeIteratorToDownNodeWithLevel<T> implements Iterator<ThreeNodeWithLevel<T>> {
    private Stack<ThreeNodeWithLevel<T>> nodes = new Stack<>();
    private ThreeNodeWithLevel<T> prev;

    public ThreeIteratorToDownNodeWithLevel(ThreeNode<T> top) {
        if (top != null) {
            nodes.push(new ThreeNodeWithLevel<>(top, 1));
        }
    }

    @Override
    public boolean hasNext() {
        return !nodes.isEmpty() || prev.getLeft() != null;
    }

    @Override
    public ThreeNodeWithLevel<T> next() {
        if (prev != null) {
            if (prev.getLeft() != null) {
                nodes.push(new ThreeNodeWithLevel<>(prev.getLeft(), prev.getLevel() + 1));
            }

            if (prev.getRight() != null) {
                nodes.push(new ThreeNodeWithLevel<>(prev.getRight(), prev.getLevel() + 1));
            }
        }

        prev = nodes.pop();
        return prev;
    }
}
