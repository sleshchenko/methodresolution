package com.tree;

import com.expression.variable.Variable;

/**
 * @author Sergii Leschenko
 */
public class ThreeNode<T> {
    private T value;
    private ThreeNode left;
    private ThreeNode right;

    public ThreeNode(T value) {
        this.value = value;
    }

    public ThreeNode(ThreeNode<T> threeNode) {
        this.value = threeNode.value;
        this.left = threeNode.left;
        this.right = threeNode.right;
    }

    public ThreeNode(T value, ThreeNode left, ThreeNode right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public ThreeNode(T value, ThreeNode left) {
        this.value = value;
        this.left = left;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public ThreeNode<T> getLeft() {
        return left;
    }

    public void setLeft(ThreeNode left) {
        this.left = left;
    }

    public ThreeNode<T> getRight() {
        return right;
    }

    public void setRight(ThreeNode right) {
        this.right = right;
    }

    public String toString() {
        return value.toString();
    }

    public ThreeNode<T> copy() {
        //TODO FIXED COPING NODES
        if (value instanceof Variable) {
            return new ThreeNode<>(value);
        }
        if (left != null && right != null) {
            return new ThreeNode<>(value, left.copy(), right.copy());
        }

        if (left != null) {
            return new ThreeNode<>(value, left.copy());
        }

        return new ThreeNode<>(value);
    }

    public void reinit(T value, ThreeNode left, ThreeNode right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public void reinit(ThreeNode<T> threeNode) {
        this.value = threeNode.value;
        this.left = threeNode.left;
        this.right = threeNode.right;
    }
}
