package com.tree;

public class ThreeNodeWithLevel<T> extends ThreeNode<T> {
    private int level;

    public ThreeNodeWithLevel(ThreeNode<T> threeNode, int level) {
        super(threeNode);
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
