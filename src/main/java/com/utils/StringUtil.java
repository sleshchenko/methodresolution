package com.utils;

/**
 * @author Sergii Leschenko
 */
public final class StringUtil {
    private StringUtil() {
    }

    public static String deleteSpaces(String source) {
        StringBuilder result = new StringBuilder();
        for (Character character : source.toCharArray()) {
            if (!character.equals(' ')) {
                result.append(character);
            }
        }
        return result.toString();
    }

    public static String revertString(String input) {
        StringBuilder result = new StringBuilder();
        for (int i = input.length() - 1; i >= 0; --i) {
            result.append(input.charAt(i));
        }
        return result.toString();
    }
}
