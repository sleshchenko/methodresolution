package com.form;

import com.expression.ExpressionItem;
import com.reducing.ConstantReducer;
import com.reducing.VariablesReducer;
import com.tree.ThreeNode;
import com.tree.Three;
import com.tree.iterator.ThreeIteratorToUp;

import java.util.Iterator;

/**
 * @author Sergii Leschenko
 */
public class ThreeReducing {
    private final VariablesReducer varReducer = new VariablesReducer();
    private final ConstantReducer constReducer = new ConstantReducer();

    public void reduce(Three<ExpressionItem> three) {
        three.forEach(varReducer::tryReduce);

        Iterator<ThreeNode> iterator = new ThreeIteratorToUp(three.getTop());
        while (iterator.hasNext()) {
            constReducer.tryReduce(iterator.next());
        }
    }
}
