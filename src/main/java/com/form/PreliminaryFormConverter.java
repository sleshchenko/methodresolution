package com.form;

import com.conversion.ArrowConverter;
import com.conversion.Converter;
import com.conversion.EquivalencyConverter;
import com.conversion.ImplicationConverter;
import com.conversion.LawDistributive;
import com.conversion.NegationConverter;
import com.conversion.StrokeConverter;
import com.conversion.XorConverter;
import com.expression.ExpressionItem;
import com.expression.operation.Operator;
import com.tree.ThreeNode;
import com.tree.Three;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Sergii Leschenko
 */
public class PreliminaryFormConverter {
    private Map<Operator, Converter> converters = new HashMap<>();
    private Map<Operator, Converter> laws       = new HashMap<>();
    private ThreeReducing            reducer    = new ThreeReducing();

    {
        converters.put(Operator.IMPLICATION, new ImplicationConverter());
        converters.put(Operator.EQUIVALENCY, new EquivalencyConverter());
        converters.put(Operator.STROKE, new StrokeConverter());
        converters.put(Operator.ARROW, new ArrowConverter());
        converters.put(Operator.XOR, new XorConverter());

        laws.put(Operator.DISJUNCTION, new LawDistributive());
    }

    public Three<ExpressionItem> convert(Three<ExpressionItem> input) {
        for (ThreeNode threeNode : input) {
            boolean converted = true;
            while (converted) {
                if (threeNode.getValue() instanceof Operator) {
                    if (converters.containsKey(threeNode.getValue())) {
                        converted = converters.get(threeNode.getValue()).convert(threeNode);
                        continue;
                    }
                }
                converted = false;
            }
        }

        for (ThreeNode<ExpressionItem> threeNode : input) {
            boolean converted = true;
            while (converted) {
                if (threeNode.getValue() instanceof Operator) {
                    if (Operator.NEGATION.equals(threeNode.getValue())) {
                        converted = new NegationConverter().convert(threeNode);
                        continue;
                    }
                }
                converted = false;
            }
        }

        for (ThreeNode threeNode : input) {
            Converter law;
            if ((law = laws.get(threeNode.getValue())) != null) {
                while (law.convert(threeNode)) ;
            }
        }

        reducer.reduce(input);

        return input;
    }
}