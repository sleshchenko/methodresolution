package com.form;

import com.conversion.MakeNegative;
import com.expression.ExpressionItem;
import com.expression.variable.LogicalConstant;
import com.expression.variable.PositiveVariable;
import com.expression.variable.Variable;
import com.expression.variable.NegativeVariable;
import com.expression.operation.Operator;
import com.tree.ThreeNode;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Sergii Leschenko
 */
public class Disjunct {
    private final Set<Variable> vars = new HashSet<>();

    public enum Type {
        INPUT,
        RESOLUTION,
        RESULT
    }

    private Type type;

    public Disjunct(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public Disjunct(Type type, Variable... vars) {
        this(type);
        this.vars.addAll(Arrays.asList(vars));
    }

    public Disjunct(Disjunct prototype, Type type) {
        this(type);
        vars.addAll(prototype.getVars());
    }

    public void removeVariables(Variable variable) {
        vars.remove(variable);
    }

    public void addVariable(Variable var) {
        vars.add(var);
    }

    public void addVariables(Collection<Variable> var) {
        vars.addAll(var);
    }

    public Set<Variable> getVars() {
        return vars;
    }

    public int getSize() {
        return vars.size();
    }

    public boolean equals(Object o) {
        if (!(o instanceof Disjunct)) {
            return false;
        }

        Disjunct disjunct = (Disjunct)o;
        if (vars.size() != disjunct.vars.size()) {
            return false;
        }

        boolean equals = true;

        Iterator<Variable> iterator = vars.iterator();
        while (equals && iterator.hasNext()) {
            if (!disjunct.vars.contains(iterator.next())) {
                equals = false;
            }
        }

        return equals;
    }

    public boolean containsVariable(Variable variable) {
        return vars.contains(variable);
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        for (Variable variable : vars) {
            result = prime * result + variable.hashCode();
        }
        return result;
    }

    public ThreeNode<ExpressionItem> toThree() {
        if (vars.isEmpty()) {
            return null;
        }

        Iterator<Variable> iterator = vars.iterator();

        if (vars.size() == 1) {
            return getNode(iterator.next());
        }

        ThreeNode<ExpressionItem> root = new ThreeNode<>(Operator.DISJUNCTION);

        ThreeNode prev = root;
        for (int i = 0; i < vars.size() - 2; ++i) {
            prev.setRight(getNode(iterator.next()));
            ThreeNode<ExpressionItem> tmp = new ThreeNode<>(Operator.DISJUNCTION);
            prev.setLeft(tmp);
            prev = tmp;
        }

        prev.setLeft(getNode(iterator.next()));

        prev.setRight(getNode(iterator.next()));

        return root;
    }

    private ThreeNode<ExpressionItem> getNode(Variable item) {
        if (item instanceof NegativeVariable) {
            ThreeNode<ExpressionItem> threeNode = new ThreeNode<>(item);
            new MakeNegative().convert(threeNode);
            return threeNode;
        }

        return new ThreeNode<>(item);
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        Iterator<Variable> iterator = vars.iterator();
        Variable current = null;
        if (!iterator.hasNext()) {
            return "□";
        }
        while (iterator.hasNext()) {
            current = iterator.next();

            if (iterator.hasNext()) {
                res.append(current.toString());
                res.append(Operator.DISJUNCTION.getSymbol());
            }
        }
        res.append(current != null ? current.toString() : null);
        return res.toString();
    }

    public boolean isTautology() {
        if (vars.contains(LogicalConstant.getTrue())) {
            return true;
        }

        for (Variable var : vars) {
            if (vars.contains(var.getNegation())) {
                return true;
            }
        }
        return false;
    }

    public boolean isExtendsFor(Disjunct disjunct) {
        boolean isBreak = false;
        for (Variable var : disjunct.getVars()) {
            if (!vars.contains(var)) {
                isBreak = true;
            }
        }
        return isBreak;
    }

    public static void main(String[] args) {
        Disjunct disjunct1 = new Disjunct(Type.INPUT, new PositiveVariable("A"), new PositiveVariable("B"), new PositiveVariable("C"));
        Disjunct disjunct = new Disjunct(Type.INPUT, new PositiveVariable("A"), new PositiveVariable("B"));

        System.out.println(disjunct.isExtendsFor(disjunct1));
    }
}
