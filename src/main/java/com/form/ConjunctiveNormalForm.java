package com.form;

import com.expression.ExpressionItem;
import com.expression.variable.Variable;
import com.expression.variable.NegativeVariable;
import com.expression.operation.Operator;
import com.tree.ThreeNode;
import com.tree.Three;
import com.tree.iterator.SubThreeIteratorBySpecificOperator;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Class for storage form conjunctive normal
 *
 * @author Sergii Leschenko
 */
public class ConjunctiveNormalForm {
    public Set<Disjunct> disjuncts;

    public ConjunctiveNormalForm(Set<Disjunct> disjuncts) {
        this.disjuncts = disjuncts;
    }

    public Set<Disjunct> getDisjuncts() {
        return disjuncts;
    }

    public static ConjunctiveNormalForm parseFromThree(Three<ExpressionItem> three, Disjunct.Type type) {
        //TODO
        PreliminaryFormConverter converter = new PreliminaryFormConverter();
        converter.convert(three);
        Set<Disjunct> result = new HashSet<>();
        Iterator<ThreeNode<ExpressionItem>> iterator =
                new SubThreeIteratorBySpecificOperator(three.getTop(), Operator.CONJUCTION);
        while (iterator.hasNext()) {
            ThreeNode<ExpressionItem> threeNode = iterator.next();
            if (Operator.DISJUNCTION.equals(threeNode.getValue())) {
                Iterator<ThreeNode<ExpressionItem>> iteratorByOperator =
                        new SubThreeIteratorBySpecificOperator(threeNode, Operator.DISJUNCTION);
                Disjunct disjunct = new Disjunct(type);
                ExpressionItem tmp;
                while (iteratorByOperator.hasNext()) {
                    ThreeNode<ExpressionItem> tempThreeNode = iteratorByOperator.next();
                    tmp = tempThreeNode.getValue();
                    if (tmp.equals(Operator.NEGATION) && tempThreeNode.getLeft().getValue() instanceof Variable) {
                        disjunct.addVariable(new NegativeVariable(tempThreeNode.getLeft().getValue().getValue()));
                    } else if (tmp instanceof Variable) {
                        disjunct.addVariable((Variable)tmp);
                    }
                }
                result.add(disjunct);
            } else {
                ExpressionItem tmp = threeNode.getValue();
                if (tmp.equals(Operator.NEGATION) && threeNode.getLeft().getValue() instanceof Variable) {
                    result.add(new Disjunct(type, new NegativeVariable(threeNode.getLeft().getValue().getValue())));
                } else if (tmp instanceof Variable) {
                    result.add(new Disjunct(type, (Variable)threeNode.getValue()));
                }
            }
        }

        return new ConjunctiveNormalForm(result);
    }

    public Three<ExpressionItem> toThree() {
        Iterator<Disjunct> iterator = disjuncts.iterator();

        if (disjuncts.isEmpty()) {
            return new Three<>(null);
        }

        if (disjuncts.size() == 1) {
            return new Three<>(iterator.next().toThree());
        }

        ThreeNode<ExpressionItem> root = new ThreeNode<>(Operator.CONJUCTION);

        ThreeNode prev = root;
        for (int i = 0; i < disjuncts.size() - 2; ++i) {
            prev.setRight(iterator.next().toThree());
            ThreeNode tmp = new ThreeNode<>(Operator.CONJUCTION);
            prev.setLeft(tmp);
            prev = tmp;
        }
        prev.setLeft(iterator.next().toThree());
        prev.setRight(iterator.next().toThree());

        return new Three<>(root);
    }
}
