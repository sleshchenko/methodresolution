package com.expression;

/**
 * @author Sergii Leschenko
 */
public interface ExpressionItem {
    public String getValue();
}
