package com.expression;

import com.expression.operation.Operations;
import com.expression.variable.PositiveVariable;
import com.tree.ThreeNode;
import com.tree.Three;

import java.util.List;
import java.util.ListIterator;

public class LogicExpressionThree extends Three<ExpressionItem> {
    public LogicExpressionThree(ThreeNode<ExpressionItem> top) {
        super(top);
    }

    public static Three<ExpressionItem> parseFromReversePolishNotation(ExpressionInString expression) {
        List<String> items = expression.getItems();
        return new Three<>(recursiveReadItem(items.listIterator(items.size())));
    }

    private static ThreeNode<ExpressionItem> recursiveReadItem(ListIterator<String> input) {
        String str = input.previous();
        if (Operations.isOperator(str)) {
            if (Operations.getOperator(str).isBinary()) {
                ThreeNode left = recursiveReadItem(input);
                ThreeNode right = recursiveReadItem(input);
                return new ThreeNode<>(Operations.getOperator(str), left, right);
            } else {
                ThreeNode left = recursiveReadItem(input);
                return new ThreeNode<>(Operations.getOperator(str), left);
            }
        }

        return new ThreeNode<>(new PositiveVariable(str));
    }

}
