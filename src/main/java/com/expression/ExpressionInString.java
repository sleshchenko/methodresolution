package com.expression;

import com.expression.operation.Operations;
import com.expression.operation.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author Sergii Leschenko
 */
public class ExpressionInString {
    private List<String> items;

    public ExpressionInString(List<String> value) {
        this.items = value;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    public ExpressionInString toReversePolishNotation() {
        Stack<String> stack = new Stack<>();
        List<String> result = new ArrayList<>();
        for (String str : items) {
            if (!Operations.isOperator(str)) {
                result.add(str);
            } else if (str.equals(Operator.OPENING_BRACKET.getSymbolAsString())) {
                stack.push(str);
            } else if (str.equals(Operator.CLOSING_BRACKET.getSymbolAsString())) {
                String tmpChar;
                while (!(tmpChar = stack.pop()).equals(Operator.OPENING_BRACKET.getSymbolAsString())) {
                    result.add(tmpChar);
                }
            } else if (stack.empty()) {
                stack.push(str);
            } else {
                int priority = Operations.getPriority(str);
                while (!stack.empty() && Operations.getPriority(stack.peek()) > priority) {
                    result.add(stack.pop());
                }
                stack.push(str);
            }
        }

        while (!stack.empty()) {
            result.add(stack.pop());
        }

        return new ExpressionInString(result);
    }

    public String toString() {
        return items.toString();
    }

    public static ExpressionInString parse(String str) {
        List<String> items = new ArrayList<>();
        for (int i = 0; i < str.length(); ) {
            Character ch = str.charAt(i);
            if (Operations.isOperator(ch.toString())) {
                items.add(ch.toString());
                ++i;
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append(ch);
                while (++i < str.length() && !Operations.isOperator((ch = str.charAt(i)).toString())) {
                    sb.append(ch);
                }
                items.add(sb.toString());
            }
        }
        return new ExpressionInString(items);
    }
}
