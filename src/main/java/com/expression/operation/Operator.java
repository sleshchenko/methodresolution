package com.expression.operation;

import com.expression.ExpressionItem;

/**
 * Enum of all available operators
 *
 * @author Sergii Leschenko
 */
public enum Operator implements ExpressionItem {
    NEGATION('¬', false, 9),
    CONJUCTION('∧', true, 8),
    DISJUNCTION('∨', true, 7),
    IMPLICATION('⇒', true, 6),
    EQUIVALENCY('⇔', true, 5),
    STROKE('|', true, 4),
    ARROW('↓', true, 3),
    XOR('⊕', true, 2),
    OPENING_BRACKET('(', true, 1),
    CLOSING_BRACKET(')', true, 0);

    private final int priority;
    private final char symbol;
    private final boolean binary;

    private Operator(char symbol, boolean binary, int priority) {
        this.symbol = symbol;
        this.binary = binary;
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public char getSymbol() {
        return symbol;
    }

    public String getSymbolAsString() {
        return String.valueOf(symbol);
    }

    public boolean isBinary() {
        return binary;
    }

    public String toString() {
        return String.valueOf(symbol);
    }

    public String getValue() {
        return String.valueOf(symbol);
    }
}