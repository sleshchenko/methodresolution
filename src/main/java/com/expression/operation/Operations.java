package com.expression.operation;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that easier to work with LogicOperator
 *
 * @author Sergii Leschenko
 */
public final class Operations {
    private final static Map<String, Operator> OPERATIONS = new HashMap<>();

    static {
        for (Operator operator : Operator.values()) {
            OPERATIONS.put(String.valueOf(operator.getSymbol()), operator);
        }
    }

    public static Map<String, Operator> getOperations() {
        return OPERATIONS;
    }

    public static Operator getOperator(String str) {
        return OPERATIONS.get(str);
    }

    public static boolean isOperator(String str) {
        return str.length() == 1 && OPERATIONS.containsKey(str);
    }

    private Operations() {
    }

    public static int getPriority(String str) {
        Operator operator;
        if ((operator = OPERATIONS.get(str)) != null) {
            return operator.getPriority();
        }
        return -1;
    }
}
