package com.expression.variable;

/**
 * @author Sergii Leschenko
 */
public class PositiveVariable extends Variable {
    public PositiveVariable(String name) {
        super(name);
    }

    public boolean equals(Object obj) {
        return obj instanceof PositiveVariable && ((Variable) obj).getName().equals(getName());

    }

    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public Variable getNegation() {
        return new NegativeVariable(getValue());
    }
}
