package com.expression.variable;

/**
 * Logical constant such as true or false
 *
 * @author Sergii Leschenko
 */
public abstract class LogicalConstant extends Variable {
    public LogicalConstant(String name) {
        super(name);
    }

    public static LogicalFalseConstant getFalse() {
        return new LogicalFalseConstant();
    }

    public static LogicalTrueConstant getTrue() {
        return new LogicalTrueConstant();
    }

    public static class LogicalFalseConstant extends LogicalConstant {
        public LogicalFalseConstant() {
            super("0");
        }

        @Override
        public Variable getNegation() {
            return new LogicalTrueConstant();
        }
    }

    public static class LogicalTrueConstant extends LogicalConstant {
        public LogicalTrueConstant() {
            super("1");
        }

        @Override
        public Variable getNegation() {
            return new LogicalFalseConstant();
        }
    }

}
