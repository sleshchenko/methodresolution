package com.expression.variable;

import com.expression.operation.Operator;

/**
 * Negation of logical variables
 *
 * @author Sergii Leschenko
 */
public class NegativeVariable extends Variable {
    public NegativeVariable(String name) {
        super(name);
        if (name.startsWith(Operator.NEGATION.getValue())) {
            setName(name.substring(1));
        } else {
            setName(name);
        }
    }

    public String toString() {
        return Operator.NEGATION.getSymbolAsString() + getName();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof NegativeVariable)) {
            return false;
        }

        return ((Variable) obj).getName().equals(getName());
    }

    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public Variable getNegation() {
        return new PositiveVariable(getValue());
    }
}
