package com.expression.variable;

import com.expression.ExpressionItem;

/**
 * @author Sergii Leschenko
 */
public abstract class Variable implements ExpressionItem {
    private String name;

    public Variable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }

    @Override
    public String getValue() {
        return String.valueOf(name);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Variable)) {
            return false;
        }

        return ((Variable) obj).getName().equals(getName());
    }

    public int hashCode() {
        return getValue().hashCode();
    }

    public abstract Variable getNegation();
}
