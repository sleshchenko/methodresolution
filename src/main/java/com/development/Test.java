package com.development;

import com.expression.ExpressionInString;
import com.expression.ExpressionItem;
import com.expression.LogicExpressionThree;
import com.expression.operation.Operator;
import com.expression.variable.PositiveVariable;
import com.form.ConjunctiveNormalForm;
import com.form.Disjunct;
import com.form.PreliminaryFormConverter;
import com.resolution.BreadthStrategy;
import com.resolution.SaturationStrategy;
import com.resolution.Strategy;
import com.tree.Three;

public class Test {


    private static String convert(String input) {
        input = input.replaceAll("or", Operator.DISJUNCTION.getSymbolAsString());
        input = input.replaceAll("and", Operator.CONJUCTION.getSymbolAsString());
        input = input.replaceAll("not", Operator.NEGATION.getSymbolAsString());
        input = input.replaceAll("arr", Operator.ARROW.getSymbolAsString());
        input = input.replaceAll("str", Operator.STROKE.getSymbolAsString());
        input = input.replaceAll("qwe", Operator.XOR.getSymbolAsString());
        return input;
    }

    public static void main(String[] args) throws Exception {
    /*
        ( -> 0
        ) -> 1
        ⇔ -> 2
        ⇒ -> 3
        ∨ -> 4
        ∧ -> 5
        ¬ -> 6
    */

        System.out.println(new PositiveVariable("A").equals(new PositiveVariable("A")));
                /*
        String source = convert("(X∨Z)∧(¬X∨Z)∧(¬Z)∧(X∨Y)");
        System.out.println(source);
        ExpressionInString polish = ExpressionInString.parse(source).toReversePolishNotation();
        System.out.println("---------------------INPUT----------------------------------------");
        Three<ExpressionItem> three = LogicExpressionThree.parseFromReversePolishNotation(polish);
        three.print();
        System.out.println("---------------------PRELIMINARY----------------------------------------");
        Three<ExpressionItem> newThree = new PreliminaryFormConverter().convert(three);
        newThree.print();
        System.out.println("---------------------CNF----------------------------------------");
        ConjunctiveNormalForm conjunctiveNormalForm = ConjunctiveNormalForm.parseFromThree(newThree, Disjunct.Type.INPUT);
        Three dnf = conjunctiveNormalForm.toThree();
        dnf.print();
        Strategy strategy = new BreadthStrategy(conjunctiveNormalForm.getDisjuncts());
        System.out.println(strategy.isBehove());
        */
    }
}
