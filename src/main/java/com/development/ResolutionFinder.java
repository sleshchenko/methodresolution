package com.development;

import com.conversion.MakeNegative;
import com.expression.ExpressionInString;
import com.expression.ExpressionItem;
import com.expression.LogicExpressionThree;
import com.form.ConjunctiveNormalForm;
import com.form.Disjunct;
import com.resolution.BreadthStrategy;
import com.resolution.DeletionStrategy;
import com.resolution.ResolutionPath;
import com.resolution.SaturationStrategy;
import com.resolution.SmallStrategy;
import com.resolution.Strategy;
import com.tree.Three;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResolutionFinder {
    private List<String>   expressions;
    private String         result;
    private ResolutionPath resolutionPath;

    public ResolutionFinder(List<String> expressions, String result) {
        this.expressions = expressions;
        this.result = result;
    }

    public ResolutionFinder(String result, String... expressions) {
        this.result = result;
        this.expressions = new ArrayList<>();
        this.expressions.addAll(Arrays.asList(expressions));
    }

    public ResolutionFinder(List<String> inputs) {
        expressions = new ArrayList<>();
        for (int i = 0; i < inputs.size() - 1; i++) {
            expressions.add(inputs.get(i));
        }

        result = inputs.get(inputs.size() - 1);
    }

    public boolean findResult(int type) {
        List<Three<ExpressionItem>> expressionsAsThree = new ArrayList<>();

        for (String expression : expressions) {
            expressionsAsThree.add(LogicExpressionThree.parseFromReversePolishNotation(
                    ExpressionInString.parse(expression).toReversePolishNotation()));
        }

        List<ConjunctiveNormalForm> conjunctiveNormalForms = new ArrayList<>();

        for (Three<ExpressionItem> nodes : expressionsAsThree) {
            conjunctiveNormalForms.add(ConjunctiveNormalForm.parseFromThree(nodes, Disjunct.Type.INPUT));
        }

        MakeNegative negationConverter = new MakeNegative();
        Three<ExpressionItem> three =
                LogicExpressionThree.parseFromReversePolishNotation(ExpressionInString.parse(result).toReversePolishNotation());
        negationConverter.convert(three.getTop());

        conjunctiveNormalForms.add(ConjunctiveNormalForm.parseFromThree(three, Disjunct.Type.RESULT));

        Strategy strategy;

        switch (type) {
            case 1:
                strategy = new SaturationStrategy(conjunctiveNormalForms);
                break;
            case 2:
                strategy = new SmallStrategy(conjunctiveNormalForms);
                break;
            case 3:
                strategy = new BreadthStrategy(conjunctiveNormalForms);
                break;
            default:
                strategy = new SaturationStrategy(conjunctiveNormalForms);
        }

        boolean result = strategy.isBehove();

        resolutionPath = strategy.getResolutionPath();
        resolutionPath.setResult(result);
        return result;
    }

    public ResolutionPath getResolutionPath() {
        return resolutionPath;
    }
}
